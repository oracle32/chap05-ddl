package DDL;

public class Application {

	/*
	 * DDL (CRATE TABLE)
테이블 만들기
CREATE TABLE 테이블명(컬럼명 자료형(크기), 컬럼명 자료형(크기), .... )

CREATE ( 만들다 )
ALTER ( 수정 )
DROP ( 삭제 )


제약조건  CONSTRAINTS 

낫널,NOT NULL		: 데이터에 NULL을 허용하지 않는다.
유니크,UNIQUE 		: 중복된 값을 허용하지 않는다.
프라이머리키,PRIMARY KEY	: NULL 허용X 중복허용 X					[기본키]
포리즌키,FOREIGN KEY 	: 참조되는 테이블의 컬럼의 값이 존재하면 허용한다.
체크,CHECK		: 저장 가능한 데이터 값의 범위나 조건을 지정하여 설정한 값만 허용한다.
CHECK -> : 음수가들어오면안돼, 5보다큰값안돼, 남자여자 둘중에하나만들어와야해,
이렇게 고정적인 내용은 CHECK를 쓴다.



CREATE TABLE MEMBER(		
  MEMBER_ID VARCHAR2(20),
  MEMBER_PWD VARCHAR2(20),
  MEMBER_NAME VARCHAR2(20)
);
-- MEMBER란  테이블을 생성되었다.


-- COMMENT ON COLUMN 테이블명.컬럼명 IS '주석내용';		
COMMENT ON COLUMN MEMBER.MEMBER_ID IS '회원아이디';
COMMENT ON COLUMN MEMBER.MEMBER_PWD IS '비밀번호';  
COMMENT ON COLUMN MEMBER.MEMBER_NAME IS '회원이름';
-- COMMENT 칸이 NULL에서 각자의 이름으로 바뀌었다. 

DESC MEMBER;    -- 내림차순아님 디스크립션 (정보확인)
-------------------------------------------------------------- 기본틀

제약조건
NOT NULL : 해당 컬럼에 반드시 값이 기록되어야 하는 경우 사용
           삽입/수정 시 NULL값을 허용하지 않도록 컬럼레벨에서 제한,
           
CREATE TABLE USR_NOCONS (
  USER_NO NUMBER,
  USER_ID VARCHAR2(20),
  USER_PWD VARCHAR2(30),
  USER_NAME VARCHAR2(30),
  GENDER VARCHAR2(10),
  PHONE VARCHAR2(30),
  EMAIL VARCHAR2(50)
);						// 이렇게 하고 컨트롤+엔터 = 옆에 USR_NOCONS테이블이 만들어진다. 
			

SELECT * FROM USER_NOCONS; 해보면 위에 USR_NOCONS 테이블이 만들어진걸 확인할수 있다.
이렇게하고 COMMIT;    &     ROLLBACK; 둘중에 하나 해줘야한다.


INSERT 				1 -- 넣어준다
  INTO USER_NOCONS	2 -- 어디에? USER_NOCONS에
(					
  USER_NO
, USER_ID
, USER_PWD
, USER_NAME			3 -- () 에 넣어주려고하는 컬럼을 적어준다.
, GENDER
, PHONE
, EMAIL  	
)  
VALUES				4 -- 3()에 넣어준 값에 값을 넣어줄대 VALUES를 써준다. 순서에 맞게,유형에맞게
(
  1
, 'user01'
, 'pass01'			5 -- 쓰고 엔터시 "1행이 삽입되었습니다"
, '홍길동'
, '남'
, '010-1234-5678'
, 'hong123@ohgiraffers.com'  
)   

SELECT * FROM USER_NOCONS; 를 다시해보면 위에 썻던( 1~5 )의 1개의 행이 다들어 오게된다. 



INSERT 				1 -- 넣어준다
  INTO USER_NOCONS	2 -- 어디에? USER_NOCONS에
(					
  USER_NO
, USER_ID
, USER_PWD
, USER_NAME			3 -- () 에 넣어주려고하는 컬럼을 적어준다.
, GENDER
, PHONE
, EMAIL  	
)  
VALUES				4 -- 3()에 넣어준 값에 값을 넣어줄대 VALUES를 써준다. 순서에 맞게,유형에맞게
(
  2
, NULL
, NULL			5 -- 쓰고 엔터시 "1행이 삽입되었습니다"
, NULL
, NULL
, '010-1234-5678'
, 'hong123@ohgiraffers.com'  
)  

이렇게하고 COMMIT;    &     ROLLBACK; 둘중에 하나 해줘야한다.


SELECT
       UC.*
  FROM USER_CONSTRAINTS UC;  -- 유저가 가지고 있는 모든 제약조건을 볼수 있다.
       
SELECT
       UCC.*
  FROM USER_CONS_COLUMNS UCC; -- 컬럼에 할당되어 있는 제약 조건을 확인할 수 있다.     


---------------위에를 확인해보면 2번에 NULL값이 4개나 있다. 그럼 이용자가 로그인을 어떻게 할것인가..
--------------- 아래에 제약조건을 걸어본다. 


CREATE TABLE USER_NOTNULL (		-- 테이블명 이름  USER_NOTNULL
  USER_NO NUMBER NOT NULL 		-- 컬럼레벨 제약조건 설정 /   USER_NO NUMBER 옆에 NOT NULL이 붙었다. 제약조건을 쓴거다
  USER_ID VARCHAR2(20) NOT NULL,
  USER_PWD VARCHAR2(30) NOT NULL,
  USER_NAME VARCHAR2(30) NOT NULL,		-- 옆에 테이블이  USER_NOTNULL이란 이름으로 만들어 졌다.
  GENDER VARCHAR2(10),
  PHONE VARCHAR2(30),
  EMAIL VARCHAR2(50)
)

SELECT
       UC.*
     , UCC.*
  FROM USER_CONSTRAINTS UC
  JOIN USER_CONS_COLUMNS UCC ON(UC.CONSTRAINT_NAME = UCC.CONSTRAINT_NAME) 		-- 모든테이블,컬럼과할께
 WHERE UC.TABLE_NAME = 'USER_NOTNULL'; 	-- 우리가 넣어놨던 이것만 찾아온다.      











 */
	
	
}
