package DDL;

public class 제약조건6가지 {
	
/*
 * 제약 조건(Constraint) 이란 데이터의 무결성을 지키기 위해 제한된 조건을 의미합니다.
 * 쉽게 말해서 테이블이나 속성에 부적절한 데이터가 들어오는 것을 사전에 차단하도록 정해놓은것이라 생각하시면 됩니다.


  -- 제약조건 조회방법 --
  SELECT 
        *
    FROM USER_CONSTRAINTS;
    
        - 조건문을 달아 줄수도있다. -
  SELECT 
        *
    FROM USER_CONSTRAINTS
   WHERE TABLE_NAME = [테이블명]; 
	
	

  CREATE TABLE TEST (

  VALUE1 VARCHAR2(10) NOT NULL,    -- NULL 값 들어오지 못하게 함
  VALUE2 VARCHAR2(10) UNIQUE,      -- NULL 허용, 중복 불가
  VALUE3 VARCHAR2(10) PRIMARY KEY, -- NULL, 중복 불가
  VALUE4 VARCHAR2(10) REFERENCES FOREIGN_TABLE (VALUE5),
  -- FOREIGN KEY는 다른 테이블에서 PRIMARY KEY에 해당하고 해당 PRIMARY KEY 컬럼에 있는 값만 사용가능
  VALUE5 VARCHAR2(10) CHECK(VALUE5 BETWEEN 1 AND 10), -- CHECK : 1과 10 사이의 값만 들어올 수 있음
  VALUE6 VARCHAR2(10) CHECK(VALUE6 IN ('A', 'B')),    -- CHECK : A 혹은 B만 들어올 수 있음
  VALUE7 VARCHAR2(10) DEFAULT '홍길동'                -- NULL 값이 들어올 경우 홍길동을 기본 삽입
    
    
    
    
  ========================================================= NOT NULL;  
  
   CREATE TABLE board(
 
      title VARCHAR(10) NOT NULL,
 
      content VARCHAR(1000) CONSTRAINT content_notnull NOT NULL,
 
      hashtag VARCHAR(100)
 
 	);


    [컬럼명] [타입]       NOT NULL
    title  VARCHAR(10) NOT NULL,
    
    [컬럼명]  [타입]          CONSTRAINT [제약조건 명]     NOT NULL
    content VARCHAR(1000) CONSTRAINT content_notnull NOT NULL,
    
    
    
    
    
  ========================================================= UNIQUE  
  CREATE TABLE board(
 
      id VARCHAR(10) UNIQUE NOT NULL,
 
      nickName VARCHAR(20) CONSTRAINT nickName_uq UNIQUE(nickName),
 
      title VARCHAR(10) NOT NULL,
 
      content VARCHAR(1000),
 
      hashtag VARCHAR(100)
 
      // CONSTRAINT nickName_uq UNIQUE (id,nickName) 
        
       
 	 *UNIQUE 특징 : NOT NULL과 같이 사용할 수있음. NOT NULL과 다르게  컬럼명 한번더 명시!* 
 	 
 
 	 
 	 
  ========================================================= PRIMARY KEY (PK)
  어떤 테이블이든 거의 무조건 하나는 들어간다 생각되는 기본키... 매우 중요
  PK는 NOT NULL + UNIQUE의 기능을 가지고 있음.
  주키, 기본키, 식별자등으로 불리고있다. 

   CREATE TABLE consTest(
 
      pkCol1 CHAR(8) PRIMARY KEY,								// 1번
 	
      pkCol2 CHAR(8),
 
      pkCol3 NUMBER CONSTRAINT consTest_pk3 PRIMARY KEY,		// 2번
 	
      CONSTRAINT pk_code PRIMARY KEY(pkCol2)					// 3번
      
      
  1. 컬럼명 옆에 바로 주키를 선언  
     pkCol1 CHAR(8) PRIMARY KEY,
     
  2. [해당컬럼] [타입] CONSTRAINT  [제약조건 명]   PRIMARY KEY     
      pkCol3 NUMBER CONSTRAINT consTest_pk3 PRIMARY KEY 
      
  3. CONSTRAINT [제약조건 명] PRIMARY KEY([컬럼명1], [컬럼명2]...... )    
     CONSTRAINT pk_code    PRIMARY KEY(pkCol2)	 
 
 
  보통 FK, PK는 3번으로 많이 쓰니 3번으로 연습하겠다.
  
   ========================================================= FOREIGN KEY (FK)
 - 외부키, 외래키, 참조키, 외부 식별자 라는 이름으로 부른다 FK 라고도 하고,
 - 참조하는 데이터 컬럼과 데이터 타입이 반드시 일치해야 한다.
 - 참조할수 있는 컬럼은 기본키(PK)이거나 UNIQUE만 가능하다.
 - 보통 PK랑 엮는게 대부분이다.
  
   CREATE TABLE parentTable(
      parentPk NUMBER PRIMARY KEY
	);
 
 
   CREATE TABLE consTest(
 
       parentPk NUMBER,
       
      pkCol1 CHAR(8),
      CONSTRAINT fk_code FOREIGN KEY(parentPk)
         REFERENCES parentTable(parentPk) ON DELETE CASCADE
 
 	);	

   자식테이블에 값을 먼저 넣을순 없다. 참조되는 컬럼에 데이터가 있어야 값을 넣을수 있다.
   
  ON DELETE CASCADE 
  참조되는 부모 테이블의 행에 대한 DELETE를 허용한다
  즉. 참조되는 부모테이블 값이 삭제되면 연쇄적으로 자식테이블 값 역시 삭제됩니다.

  ON DELETE SET NULL
 - 참조되는 부모 테이블의 행에 대한 DELETE를 허용한다.
  이건 CASCADE와 다른데 부모테이블의 값이 삭제되면 해당 참조하는 자식테이블의 값들은 NULL값으로 설정됩니다
  보통 CASCADE를 많이 사용합니다. 안그러면 하나하나 자식테이블에 있는걸 다 지워줘야 하니까요



   ========================================================= DEFAULT
   -- 아무 값을 입력하지 않아도 NULL 값이 아니라 설정한 기본값으로 자동 입력되게 하는 제약조건,
    
    CREATE TABLE board(
 
      bHit NUMBER DEFAULT 1
 
    );
    
    
    [컬럼명] [타입]  DEFAULT 기본값
    bHit   NUMBER DEFAULT 1
    이렇게 하게되면 INSERT 값을 넣어줄때 굳이 넣어주지 않아도 자동으로 1이 반영이된다.
    
    만약 테이블을 생성할때 미처 넣지 못했다 싶으면
    ALTER TABLE board MODIFY bHit DEAFULT 1; 이런식으로 넣어줘야 한다.
    
    삭제방법
    ALTER TABLE board MODIFY BHit DEFAULT NULL;
    
    
    
    
    
   ========================================================= CHECK
   - 입력 값이 조건에 맞지 않으면 DB에서 받지 않는다, 즉 [ 오류 ] 를 일으킨다.
   - 입력 값의 범위를 지정할 수 있다.
   - ex) 2000~10000까지만 입력해! 라고하면 이 설정범위를 벗어난 값은 들어올수 없다.
   
   -주의할 점은 CHECK 역시 NOT NULL처럼 나중에 조건을 추가해줄 경우 
	이미 들어가 있는 데이터가 조건에 위배되면 적용이 안됩니다.

   -ALTER TABLE [테이블명] ADD CONSTRAINT [제약조건명] [제약조건](범위)
	ALTER TABLE emp ADD CONSTRAINT emp_check CHECK(salary >= 1000 AND salary <= 10000)
	ALTER TABLE emp ADD CONSTRAINT emp_check CHECK salary IN (1000, 10000, 20000, 50000)




    
    
    
    
    
    
    
    
    


  
  
  
  
 );


  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 	 
 */

}



























































